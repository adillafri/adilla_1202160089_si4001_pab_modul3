package com.example.adilla_1202160089_si4001_pab_modul3;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adilla_1202160089_si4001_pab_modul3.R;

import java.util.ArrayList;

public class adapterdata extends RecyclerView.Adapter<adapterdata.viewHolder>{
    ArrayList<modelData> listData = new ArrayList<>();
    private adapterdata.onclick listener;

    interface onclick{
        void diklik(modelData saatIni);
    }

    public adapterdata(ArrayList<modelData> listData, adapterdata.onclick listener) {
        this.listData = listData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.per_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, final int position) {
        viewHolder.name.setText(listData.get(position).getNama());
        viewHolder.job.setText(listData.get(position).getPekerjaan());
        if (listData.get(position).getJenisKelamin().equals("Male")){
            viewHolder.gender.setImageResource(R.drawable.male);
        }else if (listData.get(position).getJenisKelamin().equals("Female")){
            viewHolder.gender.setImageResource(R.drawable.female);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { listener.diklik(listData.get(position)); }
        });
    }

    public  void addItem(modelData baru){
        listData.add(baru);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return listData.size(); }

    class viewHolder extends RecyclerView.ViewHolder{
        ImageView gender;
        TextView name, job;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            gender = itemView.findViewById(R.id.gambar_per_item);
            name = itemView.findViewById(R.id.nama_per_item);
            job = itemView.findViewById(R.id.pekerjaan_per_item);
        }
    }
}