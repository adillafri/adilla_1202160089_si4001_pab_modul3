package com.example.adilla_1202160089_si4001_pab_modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


public class dtl extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dtl);

        ((TextView)findViewById(R.id.name1)).setText(getIntent().getStringExtra("nama"));
        ((TextView)findViewById(R.id.pkj2)).setText(getIntent().getStringExtra("job"));
        if (getIntent().getStringExtra("jeniskelamin").equals("Male")){
            ((ImageView)findViewById(R.id.female1)).setImageResource(R.drawable.male);
        }else {
            ((ImageView)findViewById(R.id.female1)).setImageResource(R.drawable.female);
        }
    }
}
